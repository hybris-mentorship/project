package org.mentroship.cronjob;

import com.hybris.backoffice.excel.data.ExcelExportResult;
import com.hybris.backoffice.excel.data.SelectedAttribute;
import com.hybris.backoffice.excel.exporting.ExcelExportService;
import com.hybris.backoffice.excel.exporting.ExcelExportWorkbookPostProcessor;
import de.hybris.platform.core.model.type.AttributeDescriptorModel;
import de.hybris.platform.core.model.type.ComposedTypeModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import de.hybris.platform.servicelayer.type.TypeService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.poi.ss.usermodel.Workbook;
import org.mentorcore.model.MentorCustomerModel;
import org.mentorcore.model.UserStatisticToExcelCronJobModel;

public class UserStatisticToExcelJobPerformable extends
    AbstractJobPerformable<UserStatisticToExcelCronJobModel> {

  private static final String TYPECODE = MentorCustomerModel._TYPECODE;
  private static final String USER_STATISTICS_FOLDER_PATH =
      Paths.get("").toAbsolutePath().toString() + "/userStatistics/";
  private static final String USER_STATISTIC_FILE_PREFIX = "userStatistic_";
  private static final String EXCEL_FILE_EXTENSION = ".xlsx";

  public TypeService typeService;
  public ExcelExportService excelExportService;
  public ExcelExportWorkbookPostProcessor excelExportWorkbookPostProcessor;

  @Override
  public PerformResult perform(UserStatisticToExcelCronJobModel userStatisticToExcelCronJobModel) {
    PerformResult result = new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);

    Workbook workbook = getExcelExportService().exportData(
        TYPECODE, getSelectedAttributesForType(TYPECODE));
    ExcelExportResult excelExportResult = new ExcelExportResult(workbook);
    getExcelExportWorkbookPostProcessor().process(excelExportResult);

    createFolderIfNotExist(Paths.get(USER_STATISTICS_FOLDER_PATH));

    try {
      File file = new File(USER_STATISTICS_FOLDER_PATH
          + USER_STATISTIC_FILE_PREFIX + LocalDate.now() + EXCEL_FILE_EXTENSION);
      file.createNewFile();
      FileOutputStream fileOutputStream = new FileOutputStream(file);
      workbook.write(fileOutputStream);
    } catch (IOException e) {
      e.printStackTrace();
      result = new PerformResult(CronJobResult.ERROR, CronJobStatus.FINISHED);
    }
    return result;
  }

  private void createFolderIfNotExist(Path newFolder) {
    if (!Files.exists(newFolder)) {
      try {
        Files.createDirectories(newFolder);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  private List<SelectedAttribute> getSelectedAttributesForType(String typeCode) {
    ComposedTypeModel composedType = getTypeService().getComposedTypeForCode(typeCode);
    Set<AttributeDescriptorModel> attributeDescriptors = getTypeService()
        .getAttributeDescriptorsForType(composedType);
    return toSelectedAttributeList(attributeDescriptors);
  }

  private List<SelectedAttribute> toSelectedAttributeList(
      Set<AttributeDescriptorModel> attributeDescriptorModels) {
    return attributeDescriptorModels.stream()
        .map(SelectedAttribute::new)
        .collect(Collectors.toList());
  }

  public ExcelExportService getExcelExportService() {
    return excelExportService;
  }

  public void setExcelExportService(
      ExcelExportService excelExportService) {
    this.excelExportService = excelExportService;
  }

  public TypeService getTypeService() {
    return typeService;
  }

  public void setTypeService(TypeService typeService) {
    this.typeService = typeService;
  }

  public ExcelExportWorkbookPostProcessor getExcelExportWorkbookPostProcessor() {
    return excelExportWorkbookPostProcessor;
  }

  public void setExcelExportWorkbookPostProcessor(
      ExcelExportWorkbookPostProcessor excelExportWorkbookPostProcessor) {
    this.excelExportWorkbookPostProcessor = excelExportWorkbookPostProcessor;
  }
}
