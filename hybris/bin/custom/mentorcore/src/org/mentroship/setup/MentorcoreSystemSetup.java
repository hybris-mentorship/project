/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package org.mentroship.setup;

import static org.mentroship.constants.MentorcoreConstants.PLATFORM_LOGO_CODE;

import de.hybris.platform.core.initialization.SystemSetup;

import java.io.InputStream;

import org.mentroship.constants.MentorcoreConstants;
import org.mentroship.service.MentorcoreService;


@SystemSetup(extension = MentorcoreConstants.EXTENSIONNAME)
public class MentorcoreSystemSetup
{
	private final MentorcoreService mentorcoreService;

	public MentorcoreSystemSetup(final MentorcoreService mentorcoreService)
	{
		this.mentorcoreService = mentorcoreService;
	}

	@SystemSetup(process = SystemSetup.Process.INIT, type = SystemSetup.Type.ESSENTIAL)
	public void createEssentialData()
	{
		mentorcoreService.createLogo(PLATFORM_LOGO_CODE);
	}

	private InputStream getImageStream()
	{
		return MentorcoreSystemSetup.class.getResourceAsStream("/mentorcore/sap-hybris-platform.png");
	}
}
