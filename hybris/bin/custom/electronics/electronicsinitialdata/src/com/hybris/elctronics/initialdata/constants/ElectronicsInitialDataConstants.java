/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.hybris.elctronics.initialdata.constants;

/**
 * Global class for all ElectronicsInitialData constants.
 */
public final class ElectronicsInitialDataConstants extends GeneratedElectronicsInitialDataConstants
{
	public static final String EXTENSIONNAME = "electronicsinitialdata";

	private ElectronicsInitialDataConstants()
	{
		//empty
	}
}
