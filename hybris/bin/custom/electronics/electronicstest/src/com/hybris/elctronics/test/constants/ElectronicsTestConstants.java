/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.hybris.elctronics.test.constants;

/**
 * 
 */
public class ElectronicsTestConstants extends GeneratedElectronicsTestConstants
{

	public static final String EXTENSIONNAME = "electronicstest";

}
