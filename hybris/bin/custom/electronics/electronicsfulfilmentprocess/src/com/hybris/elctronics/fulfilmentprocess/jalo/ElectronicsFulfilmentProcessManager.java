/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.hybris.elctronics.fulfilmentprocess.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.hybris.elctronics.fulfilmentprocess.constants.ElectronicsFulfilmentProcessConstants;

public class ElectronicsFulfilmentProcessManager extends GeneratedElectronicsFulfilmentProcessManager
{
	public static final ElectronicsFulfilmentProcessManager getInstance()
	{
		ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (ElectronicsFulfilmentProcessManager) em.getExtension(ElectronicsFulfilmentProcessConstants.EXTENSIONNAME);
	}
	
}
