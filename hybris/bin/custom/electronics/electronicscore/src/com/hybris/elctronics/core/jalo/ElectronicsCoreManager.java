/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package com.hybris.elctronics.core.jalo;

import de.hybris.platform.jalo.JaloSession;
import de.hybris.platform.jalo.extension.ExtensionManager;
import com.hybris.elctronics.core.constants.ElectronicsCoreConstants;
import com.hybris.elctronics.core.setup.CoreSystemSetup;


/**
 * Do not use, please use {@link CoreSystemSetup} instead.
 * 
 */
public class ElectronicsCoreManager extends GeneratedElectronicsCoreManager
{
	public static final ElectronicsCoreManager getInstance()
	{
		final ExtensionManager em = JaloSession.getCurrentSession().getExtensionManager();
		return (ElectronicsCoreManager) em.getExtension(ElectronicsCoreConstants.EXTENSIONNAME);
	}
}
